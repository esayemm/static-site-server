// @flow

import fs from 'fs'
import AWS from 'aws-sdk'

import config from 'config'

if (config.AWS_SECRET_ACCESS_KEY || config.AWS_ACCESS_KEY_ID) {
  // Initialize the aws sdk with config values
  AWS.config.update({
    accessKeyId: config.AWS_ACCESS_KEY_ID,
    region: config.AWS_REGION,
    secretAccessKey: config.AWS_SECRET_ACCESS_KEY,
  })
}
export const s3 = new AWS.S3(
  config.AWS_ENDPOINT
    ? {
        endpoint: config.AWS_ENDPOINT,
        s3ForcePathStyle: true,
      }
    : undefined,
)

/**
 * Upload a single file to the s3 bucket.
 */
async function uploadSingleFile({
  fileName,
  filePath,
  fileType,
}: {
  fileName: string,
  filePath: string,
  fileType: string,
}): Promise<{ key: string, url: string }> {
  const params = {
    Bucket: config.AWS_BUCKET_NAME,
    Body: fs.createReadStream(filePath),
    Key: fileName,
    ContentType: fileType,
  }
  const data = await s3.upload(params).promise()

  return { key: data.Key, url: data.Location }
}

/**
 * Upload an array of files
 */
export function uploadFiles(
  files: $ReadOnlyArray<{
    key: string,
    file: { path: string, type: string },
  }>,
): Promise<Array<{ key: string, url: string }>> {
  // TODO (sam) compensate failures for uploads to s3
  return Promise.all(
    files.map(({ key, file }) =>
      uploadSingleFile({
        fileName: key,
        filePath: file.path,
        fileType: file.type,
      }),
    ),
  )
}

/**
 * IMPORTANT: DO NOT INCLUDE TRAILING SLASHES
 *
 * Copies all matching prefix keys "from" to "to".
 *
 * The below example will sync foo/bar/* to bar/*
 * ex.
 *
 * copyFilesRecursive('foo/bar', 'bar')
 *
 */
export async function syncFolders(from: string, to: string) {
  // replace trailing /
  const toPrefix = to.replace(/\/+$/, '')

  const [
    { Contents: fromContents = [], Prefix },
    { Contents: toContents = [] },
  ] = await Promise.all([
    s3
      .listObjects({
        Bucket: config.AWS_BUCKET_NAME,
        Prefix: from,
      })
      .promise(),
    s3
      .listObjects({
        Bucket: config.AWS_BUCKET_NAME,
        Prefix: to,
      })
      .promise(),
  ])

  // aws sdk will throw an error if calling this with an empty array.
  if (toContents.length > 0) {
    // Remove all content with "to" prefix.
    await s3
      .deleteObjects({
        Bucket: config.AWS_BUCKET_NAME,
        Delete: {
          Objects: toContents.map(fileInfo => ({ Key: fileInfo.Key })),
        },
      })
      .promise()
  }

  return Promise.all(
    fromContents.map(fileInfo =>
      s3
        .copyObject({
          Bucket: config.AWS_BUCKET_NAME,
          CopySource: `${config.AWS_BUCKET_NAME}/${fileInfo.Key}`,
          // Remove trailing slashes from the given "to"
          Key: `${fileInfo.Key.replace(Prefix, `${toPrefix}`)}`,
        })
        .promise(),
    ),
  )
}
