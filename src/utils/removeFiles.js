// @flow

import fs from 'fs'

/**
 * Remove an array of filepaths.
 */
export default function removeFiles(filepaths: $ReadOnlyArray<string> = []) {
  const promises = filepaths.map(
    filepath =>
      new Promise((resolve, reject) => {
        fs.unlink(filepath, err => {
          if (err) {
            reject(err)
          } else {
            resolve()
          }
        })
      }),
  )
  return Promise.all(promises)
}
