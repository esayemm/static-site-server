// @flow

import fs from 'fs'

import config from 'config'

export default function createScratchFolder() {
  if (!fs.existsSync(config.UPLOAD_TMP_DIR)) {
    fs.mkdirSync(config.UPLOAD_TMP_DIR)
  }
}
