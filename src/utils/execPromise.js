// @flow

import { exec } from 'child_process'

/**
 * Wraps node's exec function to return a Promise
 */
export default function execPromise(cmd: string): Promise<boolean> {
  return new Promise(resolve => {
    exec(cmd, (error, stdout, stderr) => {
      if (error || stderr) {
        return resolve(false)
      }
      return resolve(true)
    })
  })
}
