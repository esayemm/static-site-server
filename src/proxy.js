// @flow

import config from 'config'
import { s3 } from 'utils/aws'
import { pick } from 'ramda'

const rootDomains = config.ROOT_DOMAINS.join('|').replace('.', '\\.')
const rootRegExp = new RegExp(`^([a-zA-Z0-9_-]+)\\.(${rootDomains}/?)$`)
const idRegExp = new RegExp(
  `^([a-zA-Z0-9_-]+)\\.([a-zA-Z0-9_-]+)\\.(${rootDomains}/?)$`,
)
const fileTypeRegExp = /\.[A-Za-z_-]+$/

/**
 * This expects <app>/<id> naming convention in the s3 bucket.
 *
 * foo.robinhood.com
 *    /9012jd092d0
 *      /index.html
 *    /0192ejic29k
 *      /index.html
 *    /root
 *      /index.html
 *
 * Inbound request            S3 url
 * 1.foo.<root_domain>   ->   /foo/1/index.html
 * foo.<root_domain>     ->   /foo/root/index.html
 *
 * "root" is a reserved name "id", the reason being the <app>/root/ prefix will
 * be used to serve foo.<root_domain> style requests.
 */
function routeS3Key({ host, url }) {
  let app = ''
  let id = 'root'
  let uri = url

  // Match groups
  if (rootRegExp.test(host)) {
    const match = host.match(rootRegExp)
    app = match[1]
  } else if (idRegExp.test(host)) {
    const match = host.match(idRegExp)
    id = match[1]
    app = match[2]
  }

  // Redirect all non file type requests to index.html
  if (!fileTypeRegExp.test(url)) {
    uri = '/index.html'
  }

  return `${app}/${id}${uri}`
}

// Provide options for the proxy
export default function createProxy({
  // Proxy will bypass <host> requests that matches the provided strings
  // Array<RegExp>
  blacklist = [],
} = {}) {
  return async function s3Proxy(ctx, next) {
    if (
      ctx.request.method !== 'GET' ||
      blacklist.find(re => re.test(ctx.request.host + ctx.request.url))
    ) {
      return next()
    }

    // Turn off koa because it doesn't handle streams.
    ctx.respond = false
    const { res, req } = ctx

    const s3Key = routeS3Key({ host: req.headers.host, url: req.url })

    s3.getObject({
      Bucket: config.AWS_BUCKET_NAME,
      Key: s3Key,
      IfNoneMatch: req.headers['if-none-match'],
    })
      .on('httpHeaders', function httpHeadersHandler(statusCode, s3Headers) {
        const forwardHeaders = pick(
          ['content-type', 'last-modified', 'etag', 'cache-control'],
          s3Headers,
        )

        // make sure *.html filetypes are returned as text/html since localstack
        // s3 (local dev) is returning html as application/xml
        // https://github.com/localstack/localstack/issues/854
        if (/\.html$/.test(s3Key)) {
          forwardHeaders['content-type'] = 'text/html'
        }

        // Write the forwarded headers from s3 to the current response
        res.writeHead(statusCode, forwardHeaders)

        // Process stream from aws sdk
        // https://github.com/aws/aws-sdk-js/issues/330
        // https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/HttpResponse.html
        const stream = this.response.httpResponse.createUnbufferedStream()
        stream
          .on('error', err => {
            if (err.code === 'NotModified') {
              res.statusCode = 304
              res.end()
            } else if (err.code === 'NoSuchKey') {
              res.statusCode = 404
              res.end(err.toString())
            } else {
              res.end(err.toString())
            }
          })
          .on('data', chunk => {
            res.write(chunk)
          })
          .on('end', () => {
            res.end()
          })
      })
      .send()
  }
}
