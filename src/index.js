// @flow

import Koa from 'koa'

import createProxy from 'proxy'
import config from 'config'
import createScratchFolder from 'utils/createScratchFolder'

import router from './router'

async function main() {
  // Server init logic
  createScratchFolder()

  const app = new Koa()

  app.use(
    createProxy({
      blacklist: [/^deploy/],
    }),
  )
  app.use(router.routes())
  app.use(router.allowedMethods())

  app.listen(config.PORT)
}

main()
  .then(() => {
    console.log(`running on port ${config.PORT}`)
  })
  .catch(console.error)
