import koaBody from 'koa-body'
import Router from 'koa-router'

import config from 'config'
import { uploadFiles, syncFolders } from 'utils/aws'
import removeFiles from 'utils/removeFiles'

import pkg from '../package.json'

const router = new Router()

router.get('/', async ctx => {
  ctx.body = 'ok'
})

router.post(
  '/deploy',
  koaBody({
    multipart: true,
    formidable: {
      uploadDir: config.UPLOAD_TMP_DIR,
      // 100mb
      maxFileSize: 1000 * 1024 * 1024,
    },
  }),
  /**
   * A deploy consists of uploading the files to a subfolder in s3 and then also
   * syncing the files to the designated "root" folder. So requests to a naked url
   * will receive the newly deployed site.
   */
  async ctx => {
    const { files } = ctx.request
    const { subdomain, revisionId } = ctx.request.body
    const prefix = `${subdomain}/${revisionId}`

    try {
      await uploadFiles(
        Object.entries(files).map(([key, file]) => ({
          key: `${prefix}/${key}`,
          file,
        })),
      )
      await syncFolders(prefix, `${subdomain}/root`)

      ctx.body = {
        url: `http://${revisionId}.${subdomain}.${config.ROOT_DOMAINS[0]}`,
      }
    } catch (err) {
      throw err
    } finally {
      // Remove the temporary files on the server
      await removeFiles(
        Object.values(files).map(({ path: filepath }) => filepath),
      )
    }
  },
)

router.get('/health', async ctx => {
  ctx.body = {
    ok: true,
    version: pkg.version,
  }
})

export default router
