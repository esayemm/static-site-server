// @flow

import path from 'path'
import { pick } from 'ramda'

const config = Object.freeze({
  AWS_ACCESS_KEY_ID: '',
  // Name of s3 bucket
  AWS_BUCKET_NAME: '',
  // Use to switch between dev(localstack) and prod, provide null to hit
  // official AWS
  AWS_ENDPOINT: null,
  AWS_REGION: 'us-east-1',
  AWS_SECRET_ACCESS_KEY: '',
  // Server will use this as scratch space before uploading to s3
  UPLOAD_TMP_DIR: path.resolve(__dirname, '../.tmp'),
  PORT: 4582,
  ROOT_DOMAINS: ['local.robinhood.com:4582'],
})

// Let ENV vars override the defaults set above
export default Object.freeze({
  ...config,
  ...pick(Object.keys(config), process.env),
  ROOT_DOMAINS: process.env.ROOT_DOMAINS
    ? process.env.ROOT_DOMAINS.split(',')
    : config.ROOT_DOMAINS,
})
