# Only build this image from project root directory!

FROM ubuntu:16.04

MAINTAINER Sam L. <sam.lau@robinhood.com>

# Install system requirements.
RUN apt-get update && apt-get install -y nodejs-legacy npm curl git
# Install specific node version.
RUN npm install -g n yarn
RUN n 10

# Add source code to docker image.
WORKDIR app
ADD . .
RUN yarn install --frozen-lockfile
RUN yarn run build

ENTRYPOINT ["sh", "./entrypoint.sh"]
# CMD will be passed to yarn run <cmd>
#
# ex.
# docker run dev
#
# Will result in "yarn run dev" being called instead of the default of "serve"
CMD ["serve"]
