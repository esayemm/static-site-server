# Static Site Service

Automates static site deployment and enables quick rollbacks.

*IMPORTANT: Please use the the [CLI](https://github.com/robinhoodmarkets/static-site-service-cli) to interact with this service.*

## Table of Contents

<!-- toc -->

- [Development](#development)
  * [Requirements](#requirements)
  * [Local Development](#local-development)
  * [Useful Local Debugging Commands](#useful-local-debugging-commands)
  * [Generate Table of Content (Readme)](#generate-table-of-content-readme)
- [Deploy](#deploy)
- [Known Issues](#known-issues)
- [References](#references)

<!-- tocstop -->

## Development

### Requirements

Setup local wildcard DNS to handle requests for `*.local.robinhood.com`. This is
a good guide on setting it up locally, [Local Development with Wildcard DNS
](https://blog.thesparktree.com/local-development-with-wildcard-dns).


### Local Development

1. `make dev` - Spin up the local docker environment. (note: you can override .env variables with environmental variables [Docker Compose Env Var Guide](https://docs.docker.com/compose/environment-variables/#the-env-file))

2. `./scripts/setup-local-s3.sh` - After the s3 server is up, populate it with a bucket with the correct policies.

Now your local environment should be up and running.

`curl local.robinhood.com:4580` Should return 200


### Useful Local Debugging Commands

You can use the aws cli to interact with the localstack s3 instance, which means
all the regular aws cli commands should work. Here are some useful ones.

*Sync a local directory to a s3 bucket*

```
aws --endpoint-url="http://localhost:4572" s3 sync build s3://<URI>
```

*List recursively*

```
aws --endpoint-url="http://localhost:4572" s3 ls dev --recursive
```

### Generate Table of Content (Readme)

This will insert headers (#) and subheaders (##) between the Readme's `<!-- toc
-->` comments.

```
npx markdown-toc -i Readme.md
```


## Deploy

This is super complicated we will have to fix this later.

1. Update 'rev' with the updated SHA in [RDT DEV](https://github.com/robinhoodmarkets/robinhood-deploy-tools/blob/d8c5e38e6fd9caf5419305833261f0adec64e545/salt_master/pillar/common/tools-sites-router.sls#L6)
2. `ssh tools-sites-router.roles.dev`
3. `sudo sync_repo`
4. `highstate`

You will need to rebuild the docker image and then run the updated image, since highstate does not actually take care of rebuilding docker image + running it with the correct env vars for you.

1. `rh`
2. `cd static-site-service && docker build -t static-site-service .`
3. Run service
  ```
  docker run \
    --name deploy-server -d \
    -e AWS_ACCESS_KEY_ID="" \
    -e AWS_SECRET_ACCESS_KEY="" \
    -e AWS_BUCKET_NAME=robinhood-tools-sites \
    -e AWS_BUCKET_URI=http://robinhood-tools-sites.s3.amazonaws.com \
    -e ROOT_DOMAINS=tools.dev.robinhood.com \
    -p 80:80 \
    static-site-service
  ```

## Known Issues

- [local] Cannot access /routes locally during development (eg. visiting `v1.foo.local.service.com:4580/routes`, `v1.foo.local.service.com:4580` root works though) Might have something to do with the ports? This is not an issue in a deployed environment.

- [local] localstack s3 is returning content-type for html files as
  application/xml even though it is specified as text/html in the bucket. This
  means some pages might not load properly locally.
  https://github.com/localstack/localstack/issues/854


## References

- [AWS Doc S3 Example](https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/s3-example-photo-album.html)
- [Design Doc](https://docs.google.com/document/d/1gF-e_xH0uoESkjcwOf_dnobs2XFWrmMp4259S8I_p-c/edit#heading=h.xrs0s7zfqp1g)
