all: help

help:
	@echo ""
	@echo "deps - Install project dependencies (run on setup)"
	@echo "dev  - Run dev server"
	@echo ""

deps:
	@yarn

dev:
	@docker-compose -f docker-compose.yml down --remove-orphans
	@docker-compose -f docker-compose.yml up --remove-orphans
