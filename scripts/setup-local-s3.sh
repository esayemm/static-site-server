#!/bin/bash

# TODO (sam): get these from .env
AWS_ENDPOINT="http://localhost:4572"
AWS_BUCKET_NAME="dev"
AWS_REGION="us-east-1"

aws --endpoint="$AWS_ENDPOINT" \
  s3 mb "s3://$AWS_BUCKET_NAME"

aws s3api put-bucket-policy \
  --bucket "$AWS_BUCKET_NAME" \
  --endpoint-url="$AWS_ENDPOINT" \
  --policy "{
      \"Version\": \"2012-10-17\",
      \"Statement\": [
      {
        \"Sid\": \"PublicReadGetObject\",
        \"Effect\": \"Allow\",
        \"Principal\": \"*\",
        \"Action\": [\"s3:GetObject\"],
        \"Resource\": \"arn:aws:s3:::dev/*\"
      }
    ]
  }"
